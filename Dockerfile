
FROM debian:buster-slim

# >>>>>>>>>> ENV

ENV DEBIAN_FRONTEND noninteractive

# Définir la timezone, système / PHP
# cf: scripts/system.sh / scripts/phpfpm.sh

ENV DOCKER_TIMEZONE ''

# Par défaut, l'image est de type "production".
# Peut être surchargé lors de l'utilisation de l'image.
#   - Valeur possible: dev
#   - Valeur par défaut, ou si aucune correspondance: prod

ENV DOCKER_ENVTYPE ''

# Base de données.

ENV MARIADB_USER_DATABASE ''
ENV MARIADB_USER_NAME ''
ENV MARIADB_USER_PASSWORD ''

# Utilisateur système associé à la machine hôte.

ENV APP_USR ''
ENV APP_GRP ''
ENV APP_UID ''
ENV APP_GID ''

# Repository du projet, peut être utilisé pour instancier une production.

ENV REPO_GIT_SSH ''

# >>>>>>>>>> Utilitaires

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        vim \
        curl \
        rsync \
        wget \
        git \
        imagemagick \
        patch \
        bzip2 \
        unzip \
        openssh-client \
        gnupg \
        ca-certificates \
        apt-transport-https \
        lsb-release

# >>>>>>>>>> PHP 7.0

RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" \
        | tee /etc/apt/sources.list.d/php.list \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        php7.0-fpm \
        php7.0-xml \
        php7.0-mbstring \
        php7.0-cli \
        php7.0-curl \
        php7.0-mysql \
        php7.0-gd \
        php7.0-intl \
        php7.0-mcrypt \
        php7.0-imagick \
        php7.0-soap

COPY ./php/ini.custom.d/ /etc/php/7.0/fpm/ini.custom.d/
COPY ./php/conf.custom.d/ /etc/php/7.0/fpm/conf.custom.d/

# >>>>>>>>>> Composer

RUN curl -sS https://getcomposer.org/installer \
    | php -- --install-dir=/usr/local/bin --filename=composer

# >>>>>>>>>> Apache

RUN apt-get install -y --no-install-recommends \
        apache2 \
    && echo "\numask 002" >> /etc/apache2/envvars \
    && echo "\nServerName localhost" >> /etc/apache2/apache2.conf \
    && a2enmod \
        rewrite \
        expires \
        headers \
        userdir \
        proxy \
        proxy_fcgi \
        remoteip

COPY ./apache/index.html /var/www/html/index.html
COPY ./apache/conf-available/ /etc/apache2/conf-available/
COPY ./apache/php7.0.sock.conf /etc/apache2/php7.0.sock.conf

# >>>>>>>>>> SQL

# Inspiré du Dockerfile officiel de mariadb. Notamment pour faciliter
# l'import d'une BDD lors du démarrage initial du conteneur, et
# pour permettre la création de volume sur "/var/lib/mysql".

RUN groupadd -r mysql && useradd -r -g mysql mysql \
    ; apt-get install  -y --no-install-recommends \
        mariadb-server \
    ; rm -rf /var/lib/mysql \
    ; mkdir -p /var/lib/mysql /var/run/mysqld \
    ; chown -R mysql:mysql /var/lib/mysql /var/run/mysqld

# >>>>>>>>>> Clean

RUN apt-get autoclean \
    && apt-get autoremove --purge \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# >>>>>>>>>> Arbo & Scripts

RUN mkdir app \
    && mkdir /app/www \
    && mkdir /app/log \
    && mkdir /app/init \
    && mkdir /app/init/apache2 \
    && mkdir /app/init/app \
    && mkdir /app/init/ssh

COPY ./scripts/ /scripts/
RUN chmod 700 /scripts/*.sh

# >>>>>>>>>> Initialisation

EXPOSE 80
WORKDIR /app/www

CMD ["/scripts/run.sh"]