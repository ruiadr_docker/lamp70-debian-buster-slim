#!/bin/bash

. /scripts/common.sh

# >>>>>>>>>> Variables

FPMPATH='/etc/php/7.0/fpm'

# >>>>>>>>>> Timezone

sed -i "s~__TIMEZONE__~`_echo_ts`~g" $FPMPATH/ini.custom.d/zz-common.ini

# >>>>>>>>>> Configuration

# Création des liens symboliques permettant d'activer les
# configurations en fonction des environnements.

_create_symlink $FPMPATH/ini.custom.d/zz-common.ini $FPMPATH/conf.d/zz-common.ini

if [ "$DOCKER_ENVTYPE" = 'dev' ]; then
    _create_symlink $FPMPATH/ini.custom.d/zz-dev.ini $FPMPATH/conf.d/zz-dev.ini
else
    _create_symlink $FPMPATH/ini.custom.d/zz-prod.ini $FPMPATH/conf.d/zz-prod.ini
fi

# >>>>>>>>>> Utilisateur pour PHP-FPM

# Utilisateur & groupe associés à PHP-FPM (identique dev/prod).
# APP_USR & APP_GRP valides à ce stade, puisque testées par le script appelant "run.sh".

APP_USR=`echo "$APP_USR" | xargs`
sed -i "s~__APP_USR__~$APP_USR~g" $FPMPATH/conf.custom.d/zz-app.conf

APP_GRP=`echo "$APP_GRP" | xargs`
sed -i "s~__APP_GRP__~$APP_GRP~g" $FPMPATH/conf.custom.d/zz-app.conf

_create_symlink $FPMPATH/conf.custom.d/zz-app.conf $FPMPATH/pool.d/zz-app.conf

# >>>>>>>>>> Démarrage du service

_log "Démarrage de PHP-FPM...\n"

service php7.0-fpm start

exit $?